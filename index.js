const app = require('express')();
const cors = require('cors');
const jwt = require("jsonwebtoken");
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
  cors: {
    origins: ['http://localhost:8080']
  }
});

const corsOptions = {
  origin: 'http://localhost:8080',
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions));

app.get('/', (req, res) => {
  res.send('http://localhost:3000');
});

app.post('/users/signin', (req, res) => {
  const token = jwt.sign({ url: 'http://localhost:3000' }, 'shhhh');
  res.send({ token });
});

io.on('connection', (socket) => {
  const token = socket.handshake.auth.token;
  console.log('TOKEN', token)
  
  console.log('a user connected');
  
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
  
  socket.on('message', ({ message }) => {
    console.log(`New message ${message}`)
  });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});